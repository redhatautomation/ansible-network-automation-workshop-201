# Exercise 3 - Playbooks

## Table of Contents

- [Exercise 3 - Playbooks](#exercise-3---playbooks)
  - [Table of Contents](#table-of-contents)
  - [Objective](#objective)
  - [Guide](#guide)
    - [Step 1 - Examine Ansible Playbook](#step-1---examine-ansible-playbook)
    - [Step 2 - Execute Ansible Playbook](#step-2---execute-ansible-playbook)
    - [Step 3 - Verify configuration on router](#step-3---verify-configuration-on-router)
    - [Step 4 - Validate idempotency](#step-4---validate-idempotency)
    - [Step 5 - Modify Ansible Playbook](#step-5---modify-ansible-playbook)
    - [Step 6 - Re-run the Ansible Playbook](#step-6---re-run-the-ansible-playbook)
    - [Step 7 - Verify configuration is applied](#step-7---verify-configuration-is-applied)
  - [Exercise Challenge](#exercise-challenge)
  - [Takeaways](#takeaways)
  - [Complete](#complete)

## Objective

Use Ansible to update the configuration of routers.  This exercise will not create an Ansible Playbook, but use an existing one that has been provided. 

This exercise will cover:

* Examining an existing Ansible Playbook
* Executing an Ansible Playbook on the command line using the `ansible-navigator` command
* Stdout (`-m stdout` parameter)
* Check mode (the `--check` parameter)
* verbose mode (the `--verbose` or `-v` parameter)

## Guide

### Step 1 - Examine Ansible Playbook

Ansible Playbooks are [**YAML** files](https://yaml.org/). YAML is a structured encoding format that is also extremely human readable (unlike it's subset - the JSON format)

Navigate to the `student-repo/project_files` directory if you are not already there.  Throughout these exercises, you can examine files in either Visual Studio Code directly, or using command line utilities such as `vi` and `cat`.  

```bash
[student@ansible ~]$ cd ~/student-repo/project_files/
[student@ansible project_files]$

```
Examine the provided Ansible Playbook named `playbook.yml`. 

```yaml
---
- name: snmp ro/rw string configuration
  hosts: cisco
  gather_facts: no

  tasks:

    - name: ensure that the desired snmp strings are present
      cisco.ios.config:
        commands:
          - snmp-server community ansible-public RO
          - snmp-server community ansible-private RW
```
Here is an explanation of each line:

* The first line, `---` indicates that this is a YAML file.
* The `- name:` keyword is an optional description for this particular Ansible Playbook.
* The `hosts:` keyword means this playbook against the group `cisco` defined in the inventory file.
* The `gather_facts: no` is required since as of Ansible 2.8 and earlier, this only works on Linux hosts, and not network infrastructure.  We will use a specific module to gather facts for network equipment.
* The `tasks:` is a list of tasks with a name, and module that will do the "work" on the network device.
* `cat` - Linux command allowing us to view file contents
* `playbook.yml` - provided Ansible Playbook


This playbook will run two Cisco IOS-XE commands

```sh
snmp-server community ansible-public RO
snmp-server community ansible-private RW
```

### Step 2 - Execute Ansible Playbook

Run the playbook using the `ansible-navigator` command.  The full command is:\
```ansible-navigator run --mode stdout playbook.yml```

```bash
[student@ansible project_files]$ ansible-navigator run -m stdout playbook.yml

PLAY [snmp ro/rw string configuration] *****************************************

TASK [ensure that the desired snmp strings are present] ************************
changed: [rtr1]

PLAY RECAP *********************************************************************
rtr1                       : ok=1    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

[student@ansible project_files]$
```

* `--mode stdout` - By default `ansible-navigator` will run in interactive mode.  The default behavior can be modified by modifying the `ansible-navigator.yml`.  As playbooks get longer and involve multiple hosts the interactive mode allows you to "zoom in" on data in real-time, filter it, and navigate between various Ansible components.  Since this task only ran one task on one host the `stdout` is sufficient.

### Step 3 - Verify configuration on router

Verify that the Ansible Playbook worked.  Login to `rtr1` and check the running configuration on the Cisco IOS-XE device.

```bash
[student@ansible project_files]$ ssh rtr1

rtr1#show run | i snmp
snmp-server community ansible-public RO
snmp-server community ansible-private RW
```

### Step 4 - Validate idempotency

The `cisco.ios.config` module is idempotent. This means, a configuration change is pushed to the device if and only if that configuration does not exist on the end hosts.

> Need help with Ansible Automation terminology?  
>
> Check out the [glossary here](https://docs.ansible.com/ansible/latest/reference_appendices/glossary.html) for more information on terms like idempotency.

To validate the concept of idempotency, re-run the playbook:

```bash
[student@ansible project_files]$ ansible-navigator run --mode stdout playbook.yml

PLAY [snmp ro/rw string configuration] *****************************************

TASK [ensure that the desired snmp strings are present] ************************
ok: [rtr1]

PLAY RECAP *********************************************************************
rtr1                       : ok=1    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

> Note:
>
> See that the **changed** parameter in the **PLAY RECAP** indicates 0 changes.

Re-running the Ansible Playbook multiple times will result in the same exact output, with **ok=1** and **change=0**.  Unless another operator or process removes or modifies the existing configuration on rtr1, this Ansible Playbook will just keep reporting **ok=1** indicating that the configuration already exists and is configured correctly on the network device.

### Step 5 - Modify Ansible Playbook

Now update the task to add one more SNMP RO community string named `ansible-test`.

```sh
snmp-server community ansible-test RO
```

Use Visual Studio Code to open the `playbook.yml` file to add the command:


The Ansible Playbook will now look like this:

```yaml
---
- name: snmp ro/rw string configuration
  hosts: cisco
  gather_facts: no

  tasks:

    - name: ensure that the desired snmp strings are present
      cisco.ios.config:
        commands:
          - snmp-server community ansible-public RO
          - snmp-server community ansible-private RW
          - snmp-server community ansible-test RO
```

Make sure to save the `playbook.yml` with the change, and update Git 

```bash
git commit -a
```


### Step 6 - Re-run the Ansible Playbook

Finally re-run this playbook again without the `-v` or `--check` flag to push the changes.

```bash
[student@ansible-1 project_files]$ ansible-navigator run playbook.yml --mode stdout

PLAY [snmp ro/rw string configuration] *****************************************

TASK [ensure that the desired snmp strings are present] ************************
changed: [rtr1]

PLAY RECAP *********************************************************************
rtr1                       : ok=1    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

### Step 7 - Verify configuration is applied

Verify that the Ansible Playbook applied **ansible-test** community.  Login to `rtr1` and check the running configuration on the Cisco IOS-XE device.

```bash
[student@ansible project_files]$ ssh rtr1

rtr1#sh run | i snmp
snmp-server community ansible-public RO
snmp-server community ansible-private RW
snmp-server community ansible-test RO
```

## Exercise Challenge
Use ansible-navigator doc to review the cisco.ios.config module.  Look at the examples in the module to see how to use `parents` parameter to define the higher level command. Use the module in a playbook to set up a new standard access control list called `ansible-test` to permit 192.168.36.0 0.0.0.255. Run the playbook against rtr1, and validate the change.

Try this on your own first, but if you need help, see the [Ansible IOS Config Challenge](./ios_config_example.yml)

## Takeaways

* the **config** (e.g. cisco.ios.config) modules are idempotent, meaning they are stateful
* **verbose mode** allows us to see more output to the terminal window, including which commands would be applied
* This Ansible Playbook could be scheduled in **Automation controller** to enforce the configuration.  For example this could mean the Ansible Playbook could be run once a day for a particular network.  In combination with **check mode** this could just be a read only Ansible Playbook that sees and reports if configuration is missing or modified on the network.


## Complete

You have completed lab exercise 3 - Playbooks.

---
[Previous Exercise](../2-Inventories/README.md) | [Next Exercise](../4-VariablesAndFacts/README.md)

[Click here to return to the Ansible Network Automation Workshop](../README.md)
