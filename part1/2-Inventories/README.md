# Exercise 2 - Inventories

## Table of Contents

- [Exercise 2 - Inventories](#exercise-2---inventories)
  - [Table of Contents](#table-of-contents)
  - [Objective](#objective)
  - [Diagram](#diagram)
  - [Guide](#guide)
    - [Step 1 - Examining inventory](#step-1---examining-inventory)
      - [Static Inventory Files](#static-inventory-files)
    - [Step 2 - Using ansible-navigator to explore inventory](#step-2---using-ansible-navigator-to-explore-inventory)
    - [Step 3 - Adhoc Pings](#step-3---adhoc-pings)
  - [Exercise Challenges](#exercise-challenges)
  - [Complete](#complete)

## Objective
The objective for this exercise it to understand the use of inventories in Ansible, which are going to provide you with the endpints that you are automating.


## Diagram

![Red Hat Ansible Automation](https://github.com/ansible/workshops/raw/devel/images/ansible_network_diagram.png)

## Guide

### Step 1 - Examining inventory

#### Static Inventory Files

The scope of a `play` within a `playbook` is limited to the groups of hosts declared within an Ansible **inventory**. Ansible supports multiple [inventory](http://docs.ansible.com/ansible/latest/intro_inventory.html) types. An inventory could be a static flat file with a collection of hosts defined within it or it could be a dynamic script that querys a system such as a CMDB for a list of devices and associated variables to run a play against.

In this lab you will work with a file based static inventory written in the **ini** format. Use the `cat` command in your terminal window to view the contents of the `~/lab_inventory/hosts` file.

```bash
$ cat ~/lab_inventory/hosts
```

```ini
[all:vars]
ansible_ssh_private_key_file=~/.ssh/aws-private.pem

[routers:children]
cisco
juniper
arista

[cisco]
rtr1 ansible_host=18.222.121.247 private_ip=172.16.129.86
[arista]
rtr2 ansible_host=18.188.194.126 private_ip=172.17.158.197
rtr4 ansible_host=18.221.5.35 private_ip=172.17.8.111
[juniper]
rtr3 ansible_host=3.14.132.20 private_ip=172.16.73.175

[cisco:vars]
ansible_user=ec2-user
ansible_network_os=ios
ansible_connection=network_cli

[dc1]
rtr1
rtr3

[dc2]
rtr2
rtr4
```

In the above output every `[ ]` defines a group. For example `[dc1]` is a group that contains the hosts `rtr1` and `rtr3`. Groups can also be _nested_. The group `[routers]` is a parent group to the group `[cisco]`

Parent groups are declared using the `children` directive. Having nested groups allows the flexibility of assigining more specific values to variables.

We can associate variables to groups and hosts.

> Note:
>
> A group called **all** always exists and contains all groups and hosts defined within an inventory.

Host variables can be defined on the same line as the host themselves. For example for the host `rtr1`:

```sh
rtr1 ansible_host=18.222.121.247 private_ip=172.16.129.86
```

* `rtr1` - The name that Ansible will use.  This can but does not have to rely on DNS
* `ansible_host` - The IP address that ansible will use, if not configured it will default to DNS
* `private_ip` - This value is not reserved by ansible so it will default to a [host variable](http://docs.ansible.com/ansible/latest/intro_inventory.html#host-variables).  This variable can be used by playbooks or ignored completely.


Group variables groups are declared using the `vars` directive. Having groups allows the flexibility of assigning common variables to multiple hosts. Multiple group variables can be defined under the `[group_name:vars]` section. For example look at the group `cisco`:

```sh
[cisco:vars]
ansible_user=ec2-user
ansible_network_os=ios
ansible_connection=network_cli
```

* `ansible_user` - The user ansible will be used to login to this host, if not configured it will default to the user the playbook is run from
* `ansible_network_os` - This variable is necessary while using the `network_cli` connection type within a play definition, as we will see shortly.
* `ansible_connection` - This variable sets the [connection plugin](https://docs.ansible.com/ansible/latest/plugins/connection.html) for this group.  This can be set to values such as `netconf`, `httpapi` and `network_cli` depending on what this particular network platform supports.

We will work more with host and group variables in the next exercise.


### Step 2 - Using ansible-navigator to explore inventory

We can also use the `ansible-navigator` TUI to explore inventory.

Run the `ansible-navigator inventory` command to bring up inventory in the TUI:

![ansible-navigator tui](images/ansible-navigator.png)

Pressing **0** or **1** on your keyboard will open groups or hosts respectively.

![ansible-navigator groups](images/ansible-navigator-groups.png)

Press the **Esc** key to go up a level, or you can zoom in to an individual host:

![ansible-navigator host](images/ansible-navigator-rtr-1.png)

You can also do this in stdout mode.

```
ansible-navigator inventory -mstdout --list
```

### Step 3 - Adhoc Pings

Now we are going to have Ansible use the inventory for some basic connectivity testing.

We first need to cd to the project_files directory.

```bash
cd ~/student-repo/project_files
```

Now we can use the `ping.yml` playbook to ping groups or hosts from our inventory.  You can run `cat ping.yml` in the project_files directory to look at the playbook, which is going to use the Ansible ping module.  When you look at the playbook, you will see that we haven't specified a host or group to run the play against - it is a variable.  The `-e` is how we can specify variables (extra-vars) from the command line; here we are just specifying which host or group we want to run the playbook against.

```
ansible-navigator run -mstdout ping.yml -e "_hosts=rtr1"
```
If we are able to connect to the remote system (connect and authenticate with the endpoint) it will return a 'pong'. 

```bash
[student@ansible-1 project_files]$ ansible-navigator run -mstdout ping.yml -e "_hosts=rtr1"

PLAY [Ping a host(s) to verify connectivity] ***********************************

TASK [ping] ********************************************************************
ok: [rtr1]

TASK [debug] *******************************************************************
ok: [rtr1] => {
    "result": {
        "changed": false,
        "failed": false,
        "ping": "pong"
    }
}
```


## Exercise Challenges

Using Groups  
In the last step we were able to ping a single router.  In this challenge you need to determine how to use your inventory effectively to target specific endpoints.
* How would we ping all of the routers?
* How would we ping just the EOS routers?
* How would we ping just the JunOS routers?

Go ahead and use the ansible ping module to target each of these.


Configure a New Group and Group Vars
- Add a group to the inventory (~/lab_inventory/hosts) called cisco_sandbox.
- Add a host to the group: `sandbox-iosxe-latest-1.cisco.com`
- Create a group variables section, and define required variables
  - ansible_user=admin
  - ansible_password=Cisco12345
  - ansible_connection=network_cli
  - ansible_network_os= <what ansible_network_os setting should you use?>
- Ping the router using the Ansible ping module.

## Complete

You have completed lab exercise 2- Inventories!  

You now understand:
* Where the inventory is stored for command-line exercises
* How groups and variables are annotated in the inventory file
* How to use ansible-navigator TUI (Text-based user interface)
* How to use `ansible` ad-hoc commands with the inventory to target specifc hosts and groups

---
[Previous Exercise](../1-GettingStarted/README.md) |
[Next Exercise](../3-Playbooks/README.md)

[Click Here to return to the Ansible Network Automation Workshop](../README.md)
