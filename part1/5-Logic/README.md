# Exercise 5: Logic

## Table of Contents

- [Exercise 5: Logic](#exercise-5-logic)
  - [Table of Contents](#table-of-contents)
  - [Objective](#objective)
  - [Guide](#guide)
    - [Step 1 - Conditionals](#step-1---conditionals)
      - [Variable Based Conditional Example 1](#variable-based-conditional-example-1)
      - [Variable Based Conditional Example 2](#variable-based-conditional-example-2)
    - [Step 2 - Loops](#step-2---loops)
    - [Step 3 - Exercise Challenge](#step-3---exercise-challenge)
  - [Takeaways](#takeaways)
  - [Complete](#complete)

## Objective

We will discuss and work through examples the following in this exercise:

* Conditionals (when statements)
* Loops

## Guide

### Step 1 - Conditionals

In a playbook, you may want to execute different tasks, or have different goals, depending on the value of a fact (data about the remote system), a variable, or the result of a previous task. You may want the value of some variables to depend on the value of other variables. You can do all of these things with conditionals.  Ansible uses Jinja2 tests and filters in conditionals. Ansible supports all the standard tests and filters, and adds some unique ones as well.  See the Ansible docs for more detailed information on conditionals (https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_conditionals.html).

Conditionals can be applied to a task, or to a block of tasks.  You use the `when` clause to apply a conditional.

#### Variable Based Conditional Example 1

 In the below example we are configuring VLANs on two different devices (Juniper JunOS and Arista EOS devices; the IOS device in this workshop does not support VLANs).  

```yaml
tasks:
- name: Configure VLANs for JunOS devices
  junipernetworks.junos.junos_vlans:
    config:
    - name: vlan10
      vlan_id: 10
    - name: vlan20
      vlan_id: 20
    state: merged
  when: ansible_network_os == "junos"
  
- name: Configure VLANs for EOS devices
  arista.eos.eos_vlans:
    config:
    - name: Vlan_10
      vlan_id: 10
      state: active
    - name: Vlan_20
      vlan_id: 20
      state: active
    state: merged
  when: ansible_network_os == "eos"
```

You can see that we are applying the `when` clause at the task level to determine whether the task should run.  As discussed in an earlier exercise,the `ansible_network_os` variable required when using the network_cli connection type, and should therefore be defined as a group or host variable, which makes it ideal for use cases such as the one below.  While we will look at another way to achieve this same outcome in a later exercise, this gives you a good example of how conditionals can work for network tasks. 

Now that you have some experience with creating and running playbooks, you should be able to add these tasks to a playbook and run it using `ansible-navigator` to add the VLANs to the endpoints.  Ensure you are using the correct hosts to achieve the outcome without errors based on the inventory file.  Try on your own first, but if you need help, you can review the example file: [conditionals - example 1](conditionals_example1.yml).


#### Variable Based Conditional Example 2

Configuring endpoints to meet security compliance requirements is a routine use case for network engineering teams, so in the next example we will use a task from the DISA provided STIG Ansible content to configure our router.  We will specifically look at the DISA STIG for Cisco IOS XE (there is also DISA hosted Ansible content for Juniper JunOS and Palo Alto Networks).  You can find the content available for download on the open internet from (https://public.cyber.mil/stigs/supplemental-automation-content/).  The STIG playbook makes use of the ios_config module and is not currently written to utilize Ansible network resource modules.

Lets look at an example task from the iosxeSTIG role for the IOS-XE STIG:

```yaml
# R-215807 CISC-ND-000010
- name : stigrule_215807_ip_http_max_connections_2
  ignore_errors: "{{ ignore_all_errors }}"
  notify: "save configuration"
  ios_config:
    defaults: yes
    lines: "{{ iosxeSTIG_stigrule_215807_ip_http_max_connections_2_Lines }}"
  when:
    - iosxeSTIG_stigrule_215807_Manage
```

In this example, we have a `when` statement that will determine whether the rule is enforced based on the boolean (True/False) value of the variable.  This `when` statement is common to all rules in the STIG and enables engineering teams to easily implement the rules that are mandated by their security policy, and skip those that are not.  This tasks file is paired with a vars file where `iosxeSTIG_stigrule_215807_Manage` is defined as True or False.  The DISA STIG comes with a default file, located in the role (defaults/main.yml), which by definition includes the role defaults, but could also be used as a template for another vars file located in another location or called in such a way that it has a higher precedence (https://docs.ansible.com/ansible/latest/reference_appendices/general_precedence.html).

```yaml
# Matching content from defaults/main.yml

# R-215807 CISC-ND-000010
iosxeSTIG_stigrule_215807_Manage: False
iosxeSTIG_stigrule_215807_ip_http_max_connections_2_Lines:
  - ip http max-connections 2
```

Add the above task to a playbook and run it using `ansible-navigator` to add the rule content to the (Cisco) endpoint.  Remember that you can add variables to a playbook using the `vars` key.  Ensure you are using the correct hosts to achieve the outcome without errors based on the inventory file.  Try this on your own, but if you need help, you can review the example file:  [conditionals - example 2](conditionals_example2.yml).

### Step 2 - Loops

Another example from the DISA STIG includes a lookup, and then looping through the results of that lookup to configure vty settings for the router.

```yaml
# R-215807 CISC-ND-000010
- name: get line vty sections
  ios_command:
    commands: show running-config all | include ^line vty
  register: cmd_result
- name : stigrule_215807_session_limit_for_all_line_vty_sections
  ignore_errors: "{{ ignore_all_errors }}"
  notify: "save configuration"
  ios_config:
    defaults: yes
    lines: "{{ iosxeSTIG_stigrule_215807_session_limit_for_all_line_vty_sections_Lines }}"
    parents: "{{ item }}"
  loop: "{{ cmd_result.stdout_lines|flatten(levels=1) }}"
  when:
    - iosxeSTIG_stigrule_215807_Manage
```


### Step 3 - Exercise Challenge

The challenge is to create a playbook to use the IOS XE STIG role.  This will use some of the knowledge we reviewed in previous exercises to take a role and use it in a playbook. The IOS STIG is located in the student-repo/project_files/roles.
* Create a playbook that will use the role.
* Create the approriate variables so that all of the rules for the role are set to false, except for the banner.  If you are using a vars file, make sure to include it in the playbook.  There is a default variables file in the role.
* Create a variable in the playbook setting `ignore_all_errors: true`
* Update the banner to be a warning from a movie.
* Run the playbook against the appropriate endpoint without errors.
* Log into the router and validate the banner.

Try on your own first, but if you need help, you can review the example: [challenge 1](./challenge1_example.md).

## Takeaways

* Ansible can use conditionals to determine whether a task or group of tasks should run.
* Ansible can use loops to run a task multiple times against data in a list or dictionary.
* Ansible can be used with content already available in useful ways to make your life easier.


## Complete

You have completed lab exercise 5 on logic with Ansible.

---
[Previous Exercise](../4-VariablesAndFacts/README.md) | [Next Exercise](../6-ResourceModules/README.md)

[Click here to return to the Ansible Network Automation Workshop](../README.md)
