# Exercise 1 - Workshop Getting Started Guide

## Table of Contents

- [Exercise 1 - Workshop Getting Started Guide](#exercise-1---workshop-getting-started-guide)
  - [Table of Contents](#table-of-contents)
  - [Objective](#objective)
  - [Diagram](#diagram)
  - [Guide](#guide)
    - [Step 1 - Connecting via VS Code](#step-1---connecting-via-vs-code)
    - [Step 2 - Using the Terminal](#step-2---using-the-terminal)
    - [Step 3 - Deploy Gitea on the Control Node](#step-3---deploy-gitea-on-the-control-node)
    - [Step 4 - Open the project directory in Visual Studio Code](#step-4---open-the-project-directory-in-visual-studio-code)
    - [Step 5 - Create and check in to your repo](#step-5---create-and-check-in-to-your-repo)
  - [Complete](#complete)

## Objective

These first few lab exercises will be exploring the command-line utilities of the Ansible Automation Platform.  This includes:

- [ansible-navigator](https://github.com/ansible/ansible-navigator) - a command line utility and text-based user interface (TUI) for running and developing Ansible automation content.
- [ansible-core](https://docs.ansible.com/core.html) - the base executable that provides the framework, language and functions that underpin the Ansible Automation Platform.  It also includes various cli tools like `ansible`, `ansible-playbook` and `ansible-doc`.  Ansible Core acts as the bridge between the upstream community with the free and open source Ansible and connects it to the downstream enterprise automation offering from Red Hat, the Ansible Automation Platform.
- [Execution Environments](https://docs.ansible.com/automation-controller/latest/html/userguide/execution_environments.html) - not specifically covered in this workshop because the built-in Ansible Execution Environments already included all the Red Hat supported collections which includes all the network collections we use for this workshop.  Execution Environments are container images that can be utilized as Ansible execution.

This exercise will set up your environment for the remaining exercise in the workshop, and includes steps to set up a version control system, which is used to track and provide control over changes made to the automation code. Version control (sometimes called source control) plays an important role in any development project, including automation development. This repository will be used with both the command line utilities and when we use the Ansible Automation Platform.


## Diagram

![Red Hat Ansible Automation](https://github.com/ansible/workshops/raw/devel/images/ansible_network_diagram.png)



## Guide

### Step 1 - Connecting via VS Code

<table>
<thead>
  <tr>
    <th>It is highly encouraged to use Visual Studio Code to complete the workshop exercises. Visual Studio Code provides:
    <ul>
    <li>A file browser</li>
    <li>A text editor with syntax highlighting</li>
    <li>A in-browser terminal</li>
    </ul>
    Direct SSH access is available as a backup, or if Visual Studio Code is not sufficient to the student.  There is a short YouTube video provided if you need additional clarity: <a href="https://youtu.be/Y_Gx4ZBfcuk">Ansible Workshops - Accessing your workbench environment</a>.
</th>
</tr>
</thead>
</table>

- Connect to Visual Studio Code from the Workshop launch page (provided by your instructor).  The password is provided below the WebUI link.

  ![launch page](images/launch_page.png)

- Type in the provided password to connect.

  ![login vs code](images/vscode_login.png)


### Step 2 - Using the Terminal

- Open a terminal in Visual Studio Code (VSC):

  ![picture of new terminal](images/vscode-new-terminal.png)

### Step 3 - Deploy Gitea on the Control Node

We are going to run our first playbook here, which will deploy a Gitea server in a container on the control node (ansible-1).

In the VSC terminal, use wget to download the gitea.yml file, and then use `ansible-navigator` to run the playbook.
```
wget https://gitlab.com/redhatautomation/ansible-network-automation-workshop-201/-/raw/main/gitea/gitea.yml
```
You can run the ansible-navigator command with low verbosity, or if you want to see more of what Ansible is doing, you can add `-v` (up to 4 v's) to the ansible-navigator command.
```bash
ansible-navigator run -m stdout gitea.yml
or 
ansible-navigator run -m stdout gitea.yml -v

```
Running the gitea.yml playbook will output to the terminal all of the tasks that are done to deploy the Gitea container, configure it for use in the environment, and setup the student-repo that we will be working out of.  When the playbook has finished running, you should be able to change into the student-repo directory and run git status to see that we are up today.
```
cd student-repo
git status
```

### Step 4 - Open the project directory in Visual Studio Code

Click on the files icon in the upper right corner of your Visual Studio Code window, and click `Open Folder`.

![picture of open folder](images/open_folder.png)

In the pop-up window, choose the `/home/student/student-repo` folder, and select `OK`.

### Step 5 - Create and check in to your repo

After you have set your remote, you will be able to easily commit code in the Visual Studio Code development environment.  To validate this, create a new file in the student-repo directory. 

You can right-click on the student-repo directory in your visual studio environment, and name the file `test_file.txt`

![create a new file](images/create_new_file.png)

![write in the file](images/testfile.png)

Save the file by going to `File > Save`, or just using the `ctrl-s` key combination.  After you have done this, you can click on the Git button on the left side of the window (third down - see picture).  You will see in the Git window that there is a change to be committed.

![git](images/file_git.png)

Add in a message such as "this is just a test" and click `Commit`.  The button will change to `Sync Changes`.  Click this, and if you have set up your remote settings correctly, it will sync your change to your remote (Gitea).  If this didn't work, review the earlier steps, or ask your instructor for help.

**Optional**
You can also verify your Git Push by accessing the Gitea 'Git' repository running in your student lab pod.
Simply change the url to your student number and open the following link in a new browser tab ```https://student1.pqcfw.example.opentlc.com/gitea/

sign-in with user=gitea and password=gitea


## Complete

You have completed lab exercise 1 - Getting Started!  

You now understand:

* How to connect to the lab environment with Visual Studio Code
* How to make changes and then sync them to your remote (Gitea)
  
  

---
[Next Exercise](../2-Inventories/README.md)

[Click Here to return to the Ansible Network Automation Workshop](../README.md)