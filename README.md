# Ansible Network Automation Workshop 201 (1Day version)
**This is documentation for Ansible Automation Platform 2**

The Ansible Network Automation workshop 201 (1Day) is a full day of training an comprehensive intermediate guide to automating popular network data center devices from Arista, Cisco and Juniper via Ansible AAP. In part one of the training day, you'll deepdive into ansible network automation and levelset on key ansible techniques and best practices using ansible navigator and playbooks. In part two, you’ll spend the second part of the day exploring many popular network automation use cases that build on the morning session by applying network automation using the Ansible automation controller. 

## Presentation

Want the Presentation Deck?  Its right here:
- [Ansible Network Automation Workshop 201 Part 1 Deck](https://ansible.github.io/workshops/decks/ansible_network.pdf) PDF

- [Ansible Network Automation Workshop 201 Part 2 Deck](https://gitlab.com/redhatautomation/ansible-network-automation-workshop-201/-/blob/ansible-network-201-one-day/part2/lab_guide/Network_Automation_Workshop_Deck_201_Part2.pdf?ref_type=heads) PDF

## Ansible Network Automation Exercises
* [Part 1 ](./part1/README.md)
* [Part 2 ](./part2/README.md)

