# Part 2 Exercises
# Ansible Network Automation Workshop 201


**This is documentation for Ansible Automation Platform 2**

The Ansible Network Automation workshop 201 Part 2 is a comprehensive intermediate guide to automating popular network data center devices from Arista, Cisco and Juniper via the Ansible AAP Controller. You’ll explore many network automation use cases and learn how to apply automation using the Ansible automation controller. 

## Presentation

Want the Presentation Deck?  Its right here:
- [Ansible Network Automation Workshop 201 Part 2 Deck](https://gitlab.com/redhatautomation/ansible-network-automation-workshop-201/-/blob/ansible-network-201-one-day/part2/lab_guide/Network_Automation_Workshop_Deck_201_Part2.pdf?ref_type=heads) PDF


## Ansible Network Automation Exercises (Part2)
### Opportunistic
* [Exercise 1 - Controller as Code](./1-opportunistic/1-controller-as-code/README.md)
* [Exercise 2 - Dynamic Documentation](./1-opportunistic/2-dynamic-documentation/README.md)
### Systematic
* [Exercise 3 - Network Compliance](./2-systematic/3-network-compliance/README.md)
* [Exercise 4 - Scoped Config Management](./2-systematic/4-scoped-config-management/README.md)

## Network Diagram

![Red Hat Ansible Automation](https://github.com/ansible/workshops/blob/devel/images/ansible_network_diagram.png?raw=true)

---
![Red Hat Ansible Automation](https://github.com/ansible/workshops/blob/devel/images/rh-ansible-automation-platform.png?raw=true)


## Return Home
* [ The Main README.md](../README.md)
